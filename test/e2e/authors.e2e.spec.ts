import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/authors (GET)', () => {

    it('should return a 404 when there is no author in the DB', () => {
      return request(app.getHttpServer())
        .get('/authors')
        .expect(HttpStatus.NOT_FOUND)
    });

    it('should return a 200 when there are authors in the DB', () => {
      return request(app.getHttpServer())
        .get('/authors')
        .expect(HttpStatus.OK)
    });
  });
});
