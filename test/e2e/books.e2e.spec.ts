import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/books (GET)', () => {

    it('should return a 404 when there is no author in the DB', () => {
      return request(app.getHttpServer())
        .get('/books')
        .expect(HttpStatus.NOT_FOUND)
    });

    it('should return a 200 when there are books in the DB', () => {
      return request(app.getHttpServer())
        .get('/books')
        .expect(HttpStatus.OK)
    });
  });
});
