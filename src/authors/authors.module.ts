import { Module } from '@nestjs/common';
import { AuthorsService } from './authors.service';
import { AuthorsController } from './authors.controller';
import { LibModule } from '../lib/lib.module';

@Module({
  controllers: [AuthorsController],
  providers: [AuthorsService],
  imports: [LibModule],
})
export class AuthorsModule {}
