import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
} from '@nestjs/common';
import { AuthorsService } from './authors.service';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { Author } from './entities/author.entity';
import { ApiNotFoundResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Authors')
@Controller('authors')
export class AuthorsController {
  constructor(private readonly authorsService: AuthorsService) {}

  @Post()
  public async create(@Body() createAuthorDto: CreateAuthorDto): Promise<number> {
    return this.authorsService.create(createAuthorDto);
  }

  @ApiNotFoundResponse()
  @Get()
  public async findAll(): Promise<Author[]> {
    return this.authorsService.findAll();
  }

  @ApiNotFoundResponse()
  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Author> {
    return this.authorsService.findOne(+id);
  }

  @ApiNotFoundResponse()
  @Put(':id')
  public async update(@Param('id') id: string, @Body() updateAuthorDto: UpdateAuthorDto): Promise<void> {
    return this.authorsService.update(+id, updateAuthorDto);
  }
}
