import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { Author } from './entities/author.entity';
import { RequestService } from '../lib/request.service';

@Injectable()
export class AuthorsService {

  constructor(private readonly request: RequestService) {}

  /**
   * Create an author
   * @param {CreateAuthorDto} createAuthorDto Author to create
   * @return {Promise<number>} a Promise containing the ID of the Author created
   */
  async create(createAuthorDto: CreateAuthorDto): Promise<number> {
    const sql = `INSERT INTO author(name, firstname) VALUES($1, $2) RETURNING id_author`;
    const {id_author: idAuthor} = await this.request.execSql(sql, [createAuthorDto.name, createAuthorDto.firstname]);

    return idAuthor;
  }

  /**
   * Find all authors
   * @return {Promise<Author[]>} a Promise containing an array of Authors
   */
  async findAll(): Promise<Author[]> {
    const sql = `SELECT * FROM author ORDER BY id_author`;
    const authors: Author[] = await this.request.getRows(sql, []);

    return authors;
  }

  /**
   * Find a precise author
   * @param {number} id Id of author to find
   * @return {Promise<Author>} a Promise containing the Author searched
   */
  async findOne(id: number): Promise<Author> {
    const sql = `SELECT * FROM author WHERE id_author=$1`;
    const row: Author = await this.request.getRow(sql, [id]);

    // Author not found, throw 404 Error
    if (!row) {
      throw new NotFoundException;
    }

    return row;
  }

  /**
   * Update an author
   * @param {UpdateAuthorDto} updateAuthorDto New values for author
   * @param {number} id ID of author to update
   * @return {Promise<void>} a Promise empty
   */
  async update(id: number, updateAuthorDto: UpdateAuthorDto): Promise<void> {
     // Check if author exists
     await this.findOne(id);

     const sql = `UPDATE author SET name=$1, firstname=$2 WHERE id_author=$3`;
     await this.request.execSql(sql, [updateAuthorDto.name, updateAuthorDto.firstname, id]);
  }
}
