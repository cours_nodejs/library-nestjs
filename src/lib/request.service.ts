import { Injectable } from "@nestjs/common";
import { InjectClient } from "nest-postgres";
import { Client } from "pg";

@Injectable()
export class RequestService {

  constructor(@InjectClient() private readonly pg: Client) {}

  /**
   * Get a single row of data
   * @param {string} request Query
   * @param {String[]} parameters List of parameters
   */
  public async getRow(request: string, parameters: any[]) {
    const {rows} = await this.pg.query(request, parameters);
    return rows[0];
  }

  /**
   * Get all rows
   * @param {string} request Query
   * @param {String[]} parameters List of parameters
   */
  public async getRows(request: string, parameters: any[]) {
    const {rows} = await this.pg.query(request, parameters);
    return rows;
  }

  /**
   * Execute a sql request (UPDATE/CREATE/DELETE)
   * @param {string} request Query
   * @param {String[]} parameters List of parameters
   */
  public async execSql(request: string, parameters: any[]) {
    const {rows} = await this.pg.query(request, parameters);
    return rows[0];
  }

  public async startTransaction() {
    await this.pg.query('BEGIN');
    return;
  }

  /**
   * Commit a transaction
   */
  public async commit() {
    await this.pg.query('COMMIT');
    return;
  }

  /**
   * Rollback a transaction
   */
  public async rollback() {
    await this.pg.query('ROLLBACK');
    return;
  }

}