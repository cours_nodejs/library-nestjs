export class CreateBookDto {
  title: string;

  genre: string;

  publication: Date;

  shelf_mark: string;

  id_author: number;
}
