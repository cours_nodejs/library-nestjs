import { Module } from '@nestjs/common';
import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { LibModule } from '../lib/lib.module';

@Module({
  controllers: [BooksController],
  providers: [BooksService],
  imports: [LibModule],
})
export class BooksModule {}
