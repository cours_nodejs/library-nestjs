import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import { Book } from './entities/book.entity';
import { RequestService } from '../lib/request.service';

@Injectable()
export class BooksService {

  constructor(private readonly request: RequestService) {}

  /**
   * Create an book
   * @param {number} idLibrary ID of library to use
   * @param {CreateBookDto} createBookDto Book to create
   * @return {Promise<number>} a Promise containing the ID of the Book created
   */
  async create(idLibrary: number, createBookDto: CreateBookDto): Promise<number> {
    const sql = `
            INSERT INTO book(title, genre, shelf_mark, publication, id_author, id_library) 
            VALUES($1, $2, $3, $4, $5, $6) 
            RETURNING id_book`;
    const {id_book: idBook} = await this.request.execSql(sql, [
      createBookDto.title, createBookDto.genre, createBookDto.shelf_mark, createBookDto.publication, createBookDto.id_author, idLibrary,
    ]);

    return idBook;
  }

  /**
   * Find all books
   * @param {number} idLibrary ID of library to use
   * @return {Promise<Book[]>} a Promise containing an array of Books
   */
  async findAll(idLibrary: number): Promise<Book[]> {
    const sql = `SELECT * FROM book WHERE id_library = $1 ORDER BY id_book`;
    const rows: Book[] = await this.request.getRows(sql, [idLibrary]);

    return rows;
  }

  /**
   * Find a precise book
   * @param {number} idLibrary ID of library to use
   * @param {number} id ID of book to find
   * @return {Promise<Book>} a Promise containing the Book searched
   */
  async findOne(idLibrary: number, id: number): Promise<Book> {
    const sql = `SELECT * FROM book WHERE id_library = $1 AND id_book=$2`;
    const row: Book = await this.request.getRow(sql, [idLibrary, id]);

    // Book not found, throw 404 Error
    if (!row) {
      throw new NotFoundException()
    }

    return row;
  }

  /**
   * Update an book
   * @param {number} idLibrary ID of library to use
   * @param {number} id ID of book to update
   * @param {UpdateBookDto} updateBookDto New values for book
   * @return {Promise<void>} a Promise empty
   */
  async update(idLibrary: number, id: number, updateBookDto: UpdateBookDto): Promise<void> {
    // Check if book exists
    await this.findOne(idLibrary, id);

    const sql = `
            UPDATE book 
            SET title=$1, 
            genre=$2, 
            shelf_mark=$3, 
            publication=$4, 
            id_author=$5 
            WHERE id_library = $6 AND id_book=$7`;
    await this.request.execSql(sql, [
      updateBookDto.title, updateBookDto.genre, updateBookDto.shelf_mark, updateBookDto.publication, updateBookDto.id_author, idLibrary, id,
    ]);
  }
}
