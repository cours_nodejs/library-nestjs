import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
} from '@nestjs/common';
import { BooksService } from './books.service';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import { Book } from './entities/book.entity';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Books')
@Controller('libraries/:idLibrary/books')
export class BooksController {
  constructor(private readonly booksService: BooksService) {}

  @Post()
  public async create(@Param('idLibrary') idLibrary: string, @Body() createBookDto: CreateBookDto): Promise<number> {
    return this.booksService.create(+idLibrary, createBookDto);
  }

  @Get()
  public async findAll(@Param('idLibrary') idLibrary: string): Promise<Book[]> {
    return this.booksService.findAll(+idLibrary);
  }

  @Get(':id')
  public async findOne(@Param('idLibrary') idLibrary: string, @Param('id') id: string): Promise<Book> {
    return this.booksService.findOne(+idLibrary, +id);
  }

  @Put(':id')
  public async update(@Param('idLibrary') idLibrary: string, @Param('id') id: string, @Body() updateBookDto: UpdateBookDto): Promise<void> {
    return this.booksService.update(+idLibrary, +id, updateBookDto);
  }
}
