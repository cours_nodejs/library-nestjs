export class Book {
  id_book: number;

  title: string;

  genre: string;

  publication: Date;

  shelf_mark: string;

  id_author: number;

  id_library: number;
}
