import { Module } from '@nestjs/common';
import { AuthorsModule } from './authors/authors.module';
import { BooksModule } from './books/books.module';
import { LibrariesModule } from './libraries/libraries.module';
import { PostgresModule } from 'nest-postgres';
import postgresql from './config/postgresql';
import { LibModule } from './lib/lib.module';

@Module({
  imports: [
    AuthorsModule, BooksModule, LibrariesModule, LibModule,
    PostgresModule.forRoot(postgresql)
  ],
})

export class AppModule {}
