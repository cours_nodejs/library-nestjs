import { Module } from '@nestjs/common';
import { LibrariesService } from './libraries.service';
import { LibrariesController } from './libraries.controller';
import { LibModule } from '../lib/lib.module';

@Module({
  controllers: [LibrariesController],
  providers: [LibrariesService],
  imports: [LibModule],
})
export class LibrariesModule {}
