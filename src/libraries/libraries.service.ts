import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateLibraryDto } from './dto/create-library.dto';
import { UpdateLibraryDto } from './dto/update-library.dto';
import { Library } from './entities/library.entity';
import { RequestService } from '../lib/request.service';

@Injectable()
export class LibrariesService {

  constructor(private readonly request: RequestService) {}

  /**
   * Create an library
   * @param {CreateLibraryDto} createLibraryDto Library to create
   * @return {Promise<number>} a Promise containing the ID of the Library created
   */
  async create(createLibraryDto: CreateLibraryDto): Promise<number> {
    const sql = `INSERT INTO library(name, address) VALUES($1, $2) RETURNING id_library`;
    const {id_library: idLibrary} = await this.request.execSql(sql, [createLibraryDto.name, createLibraryDto.address]);

    return idLibrary;
  }

  /**
   * Find all libraries
   * @return {Promise<Library[]>} a Promise containing an array of Libraries
   */
  async findAll(): Promise<Library[]> {
    const sql = `SELECT * FROM library ORDER BY id_library`;
    const rows: Library[] = await this.request.getRows(sql, []);

    return rows;
  }

  /**
   * Find a precise library
   * @param {number} id Id of library to find
   * @return {Promise<Library>} a Promise containing the Library searched
   */
  async findOne(id: number): Promise<Library> {
    const sql = `SELECT * FROM library WHERE id_library=$1`;
    const row: Library = await this.request.getRow(sql, [id]);

    // Library not found, throw 404 Error
    if (!row) {
      throw new NotFoundException();
    }

    return row;
  }

  /**
   * Update an library
   * @param {UpdateLibraryDto} updateLibraryDto New values for library
   * @param {number} id ID of library to update
   * @return {Promise<void>} a Promise empty
   */
  async update(id: number, updateLibraryDto: UpdateLibraryDto): Promise<void> {
    // Check if library exists
    await this.findOne(id);

    const sql = `UPDATE library SET name=$1, address=$2 WHERE id_library=$3`;
    await this.request.execSql(sql, [updateLibraryDto.name, updateLibraryDto.address, id]);
  }
}
