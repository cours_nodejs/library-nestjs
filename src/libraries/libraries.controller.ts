import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
} from '@nestjs/common';
import { LibrariesService } from './libraries.service';
import { CreateLibraryDto } from './dto/create-library.dto';
import { UpdateLibraryDto } from './dto/update-library.dto';
import { Library } from './entities/library.entity';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Libraries')
@Controller('libraries')
export class LibrariesController {
  constructor(private readonly librariesService: LibrariesService) {}

  @Post()
  create(@Body() createLibraryDto: CreateLibraryDto) {
    return this.librariesService.create(createLibraryDto);
  }

  @Get()
  public async findAll(): Promise<Library[]> {
    return this.librariesService.findAll();
  }

  @Get(':id')
  public async findOne(@Param('id') id: string): Promise<Library> {
    return this.librariesService.findOne(+id);
  }

  @Put(':id')
  public async update(@Param('id') id: string, @Body() updateLibraryDto: UpdateLibraryDto): Promise<void> {
    return this.librariesService.update(+id, updateLibraryDto);
  }
}
