module.exports = {
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.(t|j)s',
    '!config/env/*',
    '!src/main.ts',
    '!src/**/*.(decorator|interface|schema).ts',
    '!src/**/dto/*',
    '!test/factory/**/*',
  ],
  coverageDirectory: 'coverage',
  coverageProvider: 'v8',
  coverageReporters: ['html','text'],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  preset: 'ts-jest',
  roots: [
    'src/',
    'test/'
  ],
  testEnvironment: 'node',
  testRegex: '.*\\.spec\\.ts$',
  testTimeout: 10000,
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  moduleFileExtensions: [
    'js',
    'json',
    'ts',
  ],
};
